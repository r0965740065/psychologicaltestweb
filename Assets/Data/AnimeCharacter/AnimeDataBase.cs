using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "AnimeData", menuName = "AnimeData")] 
public class AnimeDataBase : ScriptableObject
{
    [SerializeField] private new string name;
    [SerializeField] private Sprite image;

    public string Name { get => name; set => name = value; }
    public Sprite Image { get => image; set => image = value; }
}
