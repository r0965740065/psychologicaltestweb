using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataManager", menuName = "DataManager")]
public class DataManager : ScriptableObject
{
    //public List<PersonDataBase> data;
    public List<ResultDataBase> resultData;

    //public PersonDataBase GetPerson(string code)
    //{
    //    foreach (PersonDataBase p in data)
    //    {
    //        if (code == p.Code)
    //        {
    //            return p;
    //        }
    //    }

    //    Debug.Log("Can't find person of code：" + code);
    //    return null;
    //}

    public ResultDataBase GetResult(string code)
    {
        foreach (ResultDataBase p in resultData)
        {
            if (code == p.Code)
            {
                return p;
            }
        }

        Debug.Log("Can't find person of code：" + code);
        return null;
    }
}
