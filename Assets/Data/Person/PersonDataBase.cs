using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PersonType
{
    Gamer,
    Animator
}
[CreateAssetMenu(fileName = "PersonData", menuName = "PersonData")] 
public class PersonDataBase : ScriptableObject
{
    [SerializeField] private string code;
    [SerializeField] private string title;
    [SerializeField] private string titleDecoration;
    [SerializeField][TextArea(5,99)] private string description;
    [SerializeField] private Sprite myShubi;
    [SerializeField] private PersonDataBase likeShubi;
    [SerializeField] private PersonDataBase dislikeShubi;
    [SerializeField] private List<AnimeDataBase> animeCharacters = new List<AnimeDataBase>();
    [SerializeField] private PersonType type;

    public string Code { get => code;private set => code = value; }
    public string Title { get => title; private set => title = value; }
    public string TitleDecoration { get => titleDecoration; set => titleDecoration = value; }
    public string Description { get => description; private set => description = value; }
    public Sprite Shubi { get => myShubi; private set => myShubi = value; }
    public PersonDataBase LikeShubi { get => likeShubi;private set => likeShubi = value; }
    public PersonDataBase DislikeShubi { get => dislikeShubi;private set => dislikeShubi = value; }
    public List<AnimeDataBase> AnimeCharacters { get => animeCharacters; private set => animeCharacters = value; }
    public PersonType Type { get => type;private set => type = value; }
}
