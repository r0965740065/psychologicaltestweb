using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ResultData", menuName = "ResultData")]
public class ResultDataBase : ScriptableObject 
{
    [SerializeField] private string code;
    [SerializeField] private Sprite resultImage;

    public string Code { get => code; private set => code = value; }
    public Sprite ResultImage { get => resultImage; private set => resultImage = value; }
}
