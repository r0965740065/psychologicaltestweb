using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonAni : MonoBehaviour
{
    // Start is called before the first frame update
    public float delayTime;
    public float during;
    public AudioSource line;
    void OnEnable()
    {   
        transform.localScale = Vector3.zero;
        Invoke("doScale" , delayTime);
    }

    public void doScale()
    {
        if(line != null)
        {
            line.Play();
        }
        transform.DOScale(Vector3.one , during).SetEase(Ease.OutBack);  
    }

}
