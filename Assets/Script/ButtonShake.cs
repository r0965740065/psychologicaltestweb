using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ButtonShake : MonoBehaviour
{
    // Start is called before the first frame update
    public float delayTime;
    public float during;
    public float quietTime = 1f;
    public float Strength = 10f;
    void OnEnable()
    {   
        //transform.localScale = Vector3.zero;
        InvokeRepeating("doScale" , delayTime , during + quietTime);
    }

    public void doScale()
    {
        transform.DOShakeRotation(during , Strength * new Vector3(0 , 0 , 1) , 700 , 90 , true);
    }

}
