using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class ButtonTextAni : MonoBehaviour
{
    // Start is called before the first frame update
    public float during;
    public AnimationCurve animationCurve;

    void OnEnable()
    {
        doScale();
    }

    public void doScale()
    {
        GetComponent<Text>().DOFade(0, during).From(1).SetLoops(-1, LoopType.Yoyo).SetEase(animationCurve);
    }
}
