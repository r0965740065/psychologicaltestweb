using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class QuestionController : MonoBehaviour
{
    public GameObject menu;
    public GameObject[] questions;
    public GameObject resultObj;
    private int questionNum;
    public Image blackPanel;

    public string code;

    public DataManager dataManager;
    public Result result;
    /*
    public void QuestionStart()
    {
        code = "";
        questionNum = 0;
        //menu.SetActive(false);
        //questions[questionNum].SetActive(true);
        fadeAnimation();
    }   
    */
    
    public void SendCode(string code)
    {
        this.code += code;
    }

    public void NextQuestion()
    {
        questionNum++;
        if (questionNum >= questions.Length - 1)
            QuestionComplete();
        else
        {
            fadeAnimation();
        }
    }

    public void QuestionComplete()
    {
        //questions[questionNum - 1].SetActive(false);
        result.SetInformation(dataManager.GetResult(code));
        fadeAnimation();
        //resultObj.SetActive(true);
    }
    public void fadeAnimation()
    {
        blackPanel.gameObject.SetActive(true);
        blackPanel.DOFade(1 , 0.5f).From(0).OnComplete(()=>{
            questions[questionNum - 1].SetActive(false);
            questions[questionNum].SetActive(true);
            blackPanel.DOFade(0 , 0.5f).From(1).OnComplete(()=>{
                blackPanel.gameObject.SetActive(false);
        });        
        });
    }

    public void LinkFB()
    {
        Application.OpenURL("https://www.facebook.com/shubi.DMA107 ");
    }

    public void LinkIG()
    {
        Application.OpenURL("https://www.instagram.com/shu___bi/ ");
    }
}
