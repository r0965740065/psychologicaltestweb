using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public struct AnimeCharacter
{
    public GameObject gameObj;
    public Text text;
    public Image image;
}
public class Result : MonoBehaviour
{
    //public ResultDataBase data;

    public Image backGround;

    public void SetInformation(ResultDataBase data)
    {
        //this.data = data;
        backGround.sprite = data.ResultImage;
    }


    //public PersonDataBase personData;

    //[Header("Top")]
    //public Text title_top;
    //public Image shubi_top;
    //public Text description_top;

    //[Header("Center_Left")]
    //public Text title_centerLeft;
    //public Image shubi_centerLeft;

    //[Header("Center_Right")]
    //public Text title_centerRight;
    //public Image shubi_centerRight;

    //[Header("Down")]
    //public AnimeCharacter[] animeCharactersUI;

    //public void SetInformation(PersonDataBase personData)
    //{
    //    this.personData = personData;
    //    title_top.text = personData.Code + personData.Title;
    //    shubi_top.sprite = personData.Shubi;
    //    description_top.text = personData.Description;

    //    PersonDataBase likeShubi = personData.LikeShubi;
    //    title_centerLeft.text = likeShubi.Code + likeShubi.Title;
    //    shubi_centerLeft.sprite = likeShubi.Shubi;

    //    PersonDataBase dislikeShubi = personData.DislikeShubi;
    //    title_centerRight.text = dislikeShubi.Code + dislikeShubi.Title;
    //    shubi_centerRight.sprite = dislikeShubi.Shubi;

    //    animeCharactersUI[2].gameObj.SetActive(personData.AnimeCharacters.Count > 2);

    //    for(int i=0;i< personData.AnimeCharacters.Count;i++)
    //    {
    //        animeCharactersUI[i].text.text = personData.AnimeCharacters[i].Name;
    //        animeCharactersUI[i].image.sprite = personData.AnimeCharacters[i].Image;
    //    }
    //}
}
